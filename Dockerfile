FROM clamshell6412/social-media-frontend:v1

WORKDIR /app

COPY package*.json .

RUN npm install

COPY . .

RUN npm run build

CMD [ "npm", "start" ]

