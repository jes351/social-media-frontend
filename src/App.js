/* eslint-disable linebreak-style */
import React, { Component } from "react";
import "./App.css";
import Navbar from "./Components/navbar/navbar";
import { Routes, Route } from "react-router-dom";
import CardContainer from "./Components/CardContainer/CardContainer";
import CreatePost from "./Components/CreatePost/CreatePost.jsx";
import Post from "./Components/Posts/Post";
import Searchbar from "./Components/SearchBar/Searchbar";
import Profile from "./Components/Profile/Profile";
import LogOut from "./Components/LogOut/Logout";
import Search from "./Components/SearchBar/Search";
import ProtectedRoutes from "./Components/ProtectedRoutes";
import LoginScreeem from "./Components/LoginScreen/LoginScreeem";
import ValidateScreen from "./Components/ValidateScreen/ValidateScreen.jsx";

/*eslint linebreak-style: ["error", "unix"]*/

class App extends Component {
  render() {
    return (
      <>
        <Searchbar />
        <Navbar />
        <Routes>
          <Route
            path="/"
            element={<CardContainer type="random" indicator="Home" />}
          />
          <Route
            path="/SignIn"
            element={<LoginScreeem type="signin" secondType="signup" />}
          />
          <Route
            path="/Trending"
            element={<CardContainer type="trend" indicator="Popular" />}
          />
          <Route path="/search" element={<Search />} />
          <Route
            path="/Trending"
            element={<CardContainer type="trend" indicator="Popular" />}
          />
          <Route path="/Post">
            <Route path=":id/:userId" element={<Post />} />
          </Route>
          <Route element={<ProtectedRoutes />}>
            <Route
              path="/Subscriptions"
              element={
                <CardContainer type="subscriptions" indicator="Subscriptions" />
              }
            />
            <Route path="/CreatePost" element={<CreatePost type="posts" />} />
            <Route path="/Profile" element={<Profile type="profile" />} />
            <Route path="/Logout" element={<LogOut />} />
            <Route path="/Verified" element={<ValidateScreen />} />
          </Route>
        </Routes>
      </>
    );
  }
}

export default App;
