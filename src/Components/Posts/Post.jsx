import React, { useEffect, useState } from "react";
import Recommended from "../Recommended/Recommended";
import { FaFeatherAlt, FaHeart, FaHeartBroken, FaTrash } from "react-icons/fa";
import ContentComments from "../contentComments/ContentComments";
import { Link, useLocation } from "react-router-dom";
import "./Post.css";
import "../contentComments/RenderComments/Comments.css";
import {
  MdRemoveRedEye,
  MdThumbDown,
  MdThumbUp,
  MdOutlineBookmark,
  MdCoffee,
  MdThumbUpOffAlt,
  MdThumbDownOffAlt,
  MdPersonPin,
} from "react-icons/md";
import axios from "axios";
import { URL } from "../../urlStore";
import { useSelector } from "react-redux";
import Loading from "../Loading/Loading";

/*eslint linebreak-style: ["error", "unix"]*/
/* eslint-disable react/prop-types */ // TODO: upgrade to latest eslint tooling

function Post() {
  // states
  const [hideComments, setHideComments] = useState(false);
  const [hideRecommended, setHideRecommended] = useState(false);
  const [post, setPost] = useState({});
  const [user, setUser] = useState({});
  const [recommend, setRecommend] = useState([]);

  //user and post IDS
  const { currentUser } = useSelector((state) => state.user);
  const postPath = useLocation().pathname.split("/")[2];
  const userPath = useLocation().pathname.split("/")[3];
  // HEADER SET UP
  const config = {
    headers: {
      Authorization: `Bearer ${currentUser?.token}`,
      "Content-Type": "application/json",
    },
    withCredentials: true,
  };
  useEffect(() => {
    const fetchData = async () => {
      console.log("fetching");
      if (postPath) {
        try {
          const userRes = await axios.get(`${URL}users/find/${userPath}`);
          setUser(userRes.data);
          console.log(1);
        } catch (error) {
          console.log(error.message);
        }

        try {
          await axios.put(`${URL}posts/view/${postPath}`);
          console.log(2);
        } catch (error) {
          console.error(error.message);
        }
        try {
          const res = await axios.get(`${URL}posts/find/${postPath}`);
          console.log(3);
          setPost(res.data);
        } catch (error) {
          console.error(error.message);
        }
      }
    };
    fetchData();
  }, [postPath, userPath]);

  useEffect(() => {
    console.log("fetching recommend");
    const fetchRecommended = async () => {
      if (post.tags) {
        try {
          const resRecommended = await axios.get(
            `${URL}posts/tags?tags=${post.tags}`
          );
          setRecommend(resRecommended.data);
        } catch (error) {
          console.error(error.message);
        }
      } else {
        console.log("aun no carga post");
      }
    };
    fetchRecommended();
  }, [post.tags]);

  // FUNCIONES VACIAS
  const handleDeletePost = async () => {
    console.log(postPath);
    try {
      await axios.delete(`${URL}posts/deletePost/${postPath}`, {}, config);
    } catch (error) {
      console.log(error.message);
    }
  };

  const likePost = async () => {
    try {
      await axios.put(`${URL}users/like/${postPath}`, {}, config);

      console.log("lik");
    } catch (error) {
      console.error(error.message);
    }
  };

  const dislikePost = async () => {
    try {
      await axios.put(`${URL}users/dislike/${postPath}`, {}, config);
      console.log("dis");
    } catch (error) {
      console.error(error.message);
    }
  };

  const handleSubscription = async () => {
    console.log("try to sub");
    try {
      await axios.put(`${URL}users/sub/${userPath}`, {}, config);
    } catch (error) {
      console.error(error.message);
    }
  };
  const handleUnSubscription = async () => {
    try {
      await axios.put(`${URL}users/unsub/${userPath}`, {}, config);
    } catch (error) {
      console.error(error.message);
    }
  };
  return (
    <section className="section-Recommended-container">
      {!post.title ? (
        <Loading />
      ) : (
        <>
          <div className="post-section">
            <div className="story-author-container">
              <div className="head-and-description-container">
                <div className="head">
                  <h1>
                    {" "}
                    <MdOutlineBookmark /> {post.title}
                  </h1>{" "}
                  <p className="createdat-style">{post.createdAt}</p>
                </div>
                {/* si es tuyo el comentario te deja DEl & EDIT*/}

                {currentUser?.others._id === userPath ? (
                  <div className="buttons-user-actio">
                    <button className="interaction">
                      <FaFeatherAlt /> Edit
                    </button>

                    <button className="interaction" onClick={handleDeletePost}>
                      <FaTrash /> Delete
                    </button>
                  </div>
                ) : (
                  ""
                )}
              </div>

              <div className="body-post">
                <p className="parragraph-2">{post.description}</p>
              </div>
            </div>
            <div className="footer-container">
              <div className="footer__items__center bg-color">
                <div className="footer__items__center gap">
                  <Link
                    to={`/Profile/${post.userId}`}
                    className="link-list-user"
                  >
                    {/*<img className="image-author-profile" src="adsad"></img>*/}
                    <MdPersonPin className="image-author-profile" />
                  </Link>
                  <Link
                    to={`/Profile/${post.userId}`}
                    className="link-list-user"
                  >
                    <h2 className="author-name-display">{user.username}</h2>
                  </Link>
                </div>
                <div className="footer__items__center gap">
                  <FaHeart className="icon-user-info" />
                  <p className="parragraph-3">{user.subscribers}</p>
                  {console.log(user.subscribers)}
                </div>
                <div className="footer__items__center gap">
                  <MdRemoveRedEye className="icon-user-info" />
                  <p className="parragraph-4">{post.views}</p>
                </div>
              </div>
              <div className="footer__items__center">
                <button
                  className="like footer__items__center gap"
                  onClick={likePost}
                >
                  {post.like.some((id) => id === currentUser?.others._id) ? (
                    <MdThumbUp />
                  ) : (
                    <MdThumbUpOffAlt />
                  )}
                  <span>like</span>
                </button>
                <button
                  className="dislike footer__items__center gap"
                  onClick={dislikePost}
                >
                  {post.dislike.some((id) => id === currentUser?.others._id) ? (
                    <MdThumbDown />
                  ) : (
                    <MdThumbDownOffAlt />
                  )}
                  <span> dislike</span>
                </button>
                <button
                  className="Subscribe footer__items__center gap"
                  onClick={
                    user.subscribedUser.some(
                      (id) => id === currentUser?.others._id
                    )
                      ? handleUnSubscription
                      : handleSubscription
                  }
                >
                  {user.subscribedUser.some(
                    (id) => id === currentUser?.others._id
                  ) ? (
                    <div>
                      <FaHeartBroken /> <span>Unsubscribe</span>
                    </div>
                  ) : (
                    <div>
                      <FaHeart /> <span>Subscribe</span>
                    </div>
                  )}
                </button>
              </div>
              <button
                onClick={() => setHideComments(!hideComments)}
                className="hider-button"
              >
                {!hideComments ? "Hide Comments" : "Show Comments"}
              </button>
            </div>
            <div className={!hideComments ? "comentary-section" : "off"}>
              <h1 className="Discuss-coffe">
                <MdCoffee /> Discuss:{" "}
              </h1>

              <ContentComments postId={postPath} />
            </div>
            <button
              onClick={() => setHideRecommended(!hideRecommended)}
              className="hider-button"
            >
              {!hideRecommended ? "Hide Recomendations" : "Show Recomendations"}
            </button>
          </div>
          <div className="Recommended-section-node">
            <Recommended
              prop={hideRecommended}
              recommend={recommend}
              setRecommend={setRecommend}
              post={post}
            />
          </div>
        </>
      )}
    </section>
  );
}
export default Post;
