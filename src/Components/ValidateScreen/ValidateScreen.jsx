import React from "react";
import { MdCoffee } from "react-icons/md";
import { useSelector } from "react-redux";
import "./ValidateScreen.css";

function ValidateScreen() {
  const { currentUser } = useSelector((state) => state.user);
  console.log(currentUser);
  return (
    <section className="validateScren-section">
      <div className="validatescreen">
        <MdCoffee className='coffe-verified'/>
        <h1 className='welcome-verified'>Welcome {currentUser.others.username}!</h1>
        <p className='text-verified'> your account now is verified !</p>
      </div>
    </section>
  );
}
export default ValidateScreen;
