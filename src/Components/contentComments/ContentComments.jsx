import React, { useEffect, useState } from "react";
import CreateComment from "./coments/CreateComment";
import "./index.css";
import axios from "axios";
import { URL } from "../../urlStore";
import Comments from "./RenderComments/Comments";

/*eslint linebreak-style: ["error", "unix"]*/
/* eslint-disable react/prop-types */ // TODO: upgrade to latest eslint tooling

const ContentComments = ({ postId}) => {
  const [loadComments, setLoadComments] = useState([]);
  const [newComment, setNewComment] = useState({})

  useEffect(() => {
      axios.get(`${URL}comments/${postId}`)
      .then(res => {
        setLoadComments(res.data)
      })
      .catch(err => console.log(err))
  }, [newComment]);
  return (
    <div className="content">
      <CreateComment setNewComment={setNewComment} id={postId} />
      {loadComments.map((value) => {
        const { description, userId, like, dislike, createdAt } = value;
        return (
			<Comments
            id={value._id}
            key={value._id}
            description={description}
            userId={userId}
            like={like}
            dislike={dislike}
            createdat={createdAt}
          />
        );
      })}
    </div>
  );
};

export default ContentComments;
