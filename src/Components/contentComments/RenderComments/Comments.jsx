import axios from "axios";
import React, { useEffect, useRef, useState } from "react";
import { useSelector } from "react-redux";
import { Link } from "react-router-dom";
import { MdThumbDown, MdThumbUp, MdPersonPin } from "react-icons/md";
import { FaFeatherAlt, FaTrash } from "react-icons/fa";
import "./Comments.css";
import { URL } from "../../../urlStore";
import { toast } from "react-toastify";

/* eslint react/prop-types: 0 */

const Comments = ({id, createdat, userId, description,like,dislike}) => {
  const [user, setUser] = useState({});
  const { currentUser } = useSelector((state) => state.user);
  const token = currentUser?.token;
  const header = {
    headers: { Authorization: `Bearer ${token}` },
  };
  {
    /* nose como convertir el campo en editable y obtener los valores del campo*/
  }

  {
    /* CAMPOS VALIDADOS POR TOKEN */
  }
  const btns = useRef();

  useEffect(() => {
    const fetchData = async () => {
      if (userId) {
        try {
          const userRes = await axios.get(`${URL}users/find/${userId}`);
          setUser(userRes.data);
        } catch (error) {
          console.log(error.message);
        }
      }
    };
    fetchData();
  }, [userId]);


  const handleChangeComment = () => {
    const userId = currentUser._id;
    axios.put(`${URL}comments/`, { userId });
  };
  const handleLikeComment = () => {
    axios
      .put(
        `${URL}users/likeComment/${id}`,
        {},
        { withCredentials: true },
        header
      )
      .then((res) => toast.success(`Like succes ${res.data}`))
      .catch((err) => {
        toast.error(err.response.data);
      });
  };
  const handleDeleteComment = () => {
    axios
      .delete(`${URL}comments/${id}`, { withCredentials: true }, header)
      .then((res) => {
        console.log(res.data);
        toast.success(res.data.message);
      })
      .catch((err) => {
        console.log(err);
        toast.error(`Error ${err.response.data.error.message}`);
      });
  };

  const handleDislikeComment = () => {
    axios
      .put(
        `${URL}users/dislikeComment/${id}`,
        {},
        { withCredentials: true },
        header
      )
      .then(() => toast.warning("Dislike"))
      .catch((err) => toast.error(`Error ${err.response.data.error.message}`));
  };
		console.log(userId);
		console.log(user);

  return (
    <div className="comment-render">
      <div className="user-data-container">
        <Link to={`/Profile/${userId}`} className="link-list-user">
          {/*<img src="adasda" className="profile-comment-picture" />*/}
          <MdPersonPin className="profile-comment-picture" />
        </Link>
        <Link to={`/Profile/${userId}`} className="link-list-user">
          <h2>{user?.username}</h2>
        </Link>
      </div>
      <div className="comment-render-container">
        <p className="comment-parragraph">{description}</p>
        <div className="ago-and-actions">
          <p>{createdat}</p>
          {/* si es tuyo el comentario te deja DEl & EDIT*/}

          {currentUser?.others._id === userId ? (
            <div className="buttons-user-action user-auth">
              <button
                className="interaction edit"
                onClick={handleChangeComment}
              >
                <FaFeatherAlt /> Edit
              </button>
              <button
                className="interaction delete"
                onClick={handleDeleteComment}
              >
                <FaTrash /> Delete{" "}
              </button>
            </div>
          ) : (
            ""
          )}
          <div ref={btns} className="buttons-user-action">
            <button className="interaction" onClick={handleLikeComment}>
              <MdThumbUp />{like.lenght}
            </button>
            <button className="interaction" onClick={handleDislikeComment}>
              <MdThumbDown />{dislike.lenght}
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Comments;
