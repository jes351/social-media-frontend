import React from "react";
import "./index.css";
import { MdCoffee } from "react-icons/md";
import axios from "axios";
import { URL } from "../../../urlStore";
import { useSelector } from "react-redux";
import { useForm } from "react-hook-form";
import { toast } from "react-toastify";

/*eslint linebreak-style: ["error", "unix"]*/
/* eslint react/prop-types: 0 */

const CreateComment = ({ id,setNewComment }) => {
  const { currentUser } = useSelector((state) => state.user);
  const { handleSubmit, register, reset } = useForm();

  const token = currentUser?.token
  const header = {
    headers: {Authorization: `Bearer ${token}`}
  }

  const submit = data => {
    if (Object.values(data)[0].length > 2) {
      axios.post(`${URL}comments/`, { ...data, userId: currentUser.others._id, postId: id }, {withCredentials: true}, header)
      .then(res => {
        setNewComment(res.data)
        toast.success('Comment post succes!')
      })
      .catch(err => toast.error(err.response.data))
    }
    reset({
      description: ''
    })
  }
  return (
    <section className="comment">
      {/* 
      <div className="comment-head">		
        <img className="user-profile-image" alt="" src="adasdasd" />
      </div>
				*/}

      <form action="" className="comment-form" onSubmit={handleSubmit(submit)}>
        <textarea
          className="coment-form-input"
          placeholder="What is your opinion?"
          rows="5"
          {...register('description')}
        />
        <button className="comment-form-btn"><MdCoffee /> Discuss</button>
      </form>
    </section>
  );
};

export default CreateComment;
