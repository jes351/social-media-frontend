import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  posts: [],
  loading: false,
  error: null,
};

const postSlice = createSlice({
  name: "posts",
  initialState,
  reducers: {
    getPostsStart: (state) => {
      state.loading = true;
      state.error = null;
    },
    getPostsSuccess: (state, { payload }) => {
      state.loading = false;
      state.posts = payload;
    },
    getPostsFailure: (state, { payload }) => {
      state.loading = false;
      state.error = payload;
    },
    likePostSuccess: (state, { payload }) => {
      const postIndex = state.posts.findIndex(
        (post) => post._id === payload.postPath
      );
      console.log("index", postIndex);
      console.log("payload", payload);
      console.log("payload_id" , payload.postPath);
      if (postIndex !== -1) {
        // Remove the user ID from the dislike array if it exists
        state.posts[postIndex].dislike = state.posts[postIndex].dislike.filter(
          (id) => id !== payload.userPath
        );

        // Add the user ID to the like array
        state.posts[postIndex].like.push(payload.userPath);
        console.log("state.posts", state.posts);
        console.log("payload.userId", payload.userPath);
      }
    },
    dislikePostSuccess: (state, { payload }) => {
      const postIndex = state.posts.findIndex(
        (post) => post._id === payload.postPath
      );
      console.log("postIndex", postIndex);
      console.log("payload", payload);
      if (postIndex !== -1) {
        // Remove the user ID from the like array if it exists
        state.posts[postIndex].like = state.posts[postIndex].like.filter(
          (id) => id !== payload.userPath
        );

        // Add the user ID to the dislike array
        state.posts[postIndex].dislike.push(payload.userPath);
        console.log("state.posts", state.posts);
        console.log("payload.userId", payload.userPath);
      }
    },
  },
});

export const {
  getPostsStart,
  getPostsSuccess,
  getPostsFailure,
  likePostSuccess,
  dislikePostSuccess,
} = postSlice.actions;

export default postSlice.reducer;
